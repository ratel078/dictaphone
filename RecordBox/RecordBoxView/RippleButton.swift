//
//  RippleButton.swift
//  RecordBox
//
//  Created by Sergiy Bekker on 17.11.17.
//  Copyright © 2017 SBApps. All rights reserved.
//

import UIKit
import ZFRippleButton

protocol RippleButtonDelegate : NSObjectProtocol {
    func callbackClicked(_ obj : RippleButton)
    //func callbackClicked(_ obj : RippleButton)
}

class RippleButton: UIView {

    @IBOutlet weak var rippleBut : ZFRippleButton!
    @IBOutlet weak var button : UIButton!
    
    var state : Int!
    weak var delegate: RippleButtonDelegate?
   
    var rippleColor: UIColor =   UIColor(red: 239.0 / 255.0, green: 92.0 / 255.0, blue: 146.0 / 255.0, alpha: 0.2) {
        didSet {
            rippleBut.rippleColor = rippleColor
        }
    }
    var buttonColor: UIColor  = UIColor(red: 239.0 / 255.0, green: 92.0 / 255.0, blue: 146.0 / 255.0, alpha: 0.9) {
        didSet {
            button.backgroundColor = buttonColor
            var image = UIImage(named: "record")
           
            if state != nil {
                if state == State.Play.rawValue || state == State.FinishPlay.rawValue{
                    image = UIImage(named: "pause")
                } else if state == State.Pause.rawValue || state == State.FinishPause.rawValue{
                    image = UIImage(named: "play")
                }
                button.setImage(image, for: .normal)
                button.setImage(image, for: .highlighted)
            }
        }
    }
    var animationTime: Double = 0.9 {
        didSet {
            rippleBut.animationTime = animationTime
        }
    }
    
    func animate(_ state : Bool ) {
        rippleBut.animate(state)
    }
    
    @IBAction func clicked(_ sender : UIButton){
   
        self.delegate?.callbackClicked(self)
    }
}
