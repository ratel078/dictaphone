//
//  RecordBoxView.swift
//  RecordBox
//
//  Created by Sergiy Bekker on 17.11.17.
//  Copyright © 2017 SBApps. All rights reserved.
//


import UIKit
import AVFoundation

enum State : Int {
    case Record = 0
    case FinishRecord
    case Play
    case FinishPlay
    case Pause
    case FinishPause
}

let defaultRecordCollor = UIColor(red: 239.0 / 255.0, green: 92.0 / 255.0, blue: 146.0 / 255.0, alpha: 0.9)
let defaultPlayCollor = UIColor.blue.withAlphaComponent(0.9)
let defaultPauseCollor = UIColor.blue.withAlphaComponent(0.9)

protocol RecordBoxViewDelegate : NSObjectProtocol {
    func callbackWithTime(_ string : String, _ duration : TimeInterval)
    func callbackWithURL(_ url : URL)
    func callbackBeginRecord(_ obj : RecordBoxView)
    func callbackEndRecord(_ obj : RecordBoxView)
    func callbackPlay(_ obj : RecordBoxView)
    func callbackPause(_ obj : RecordBoxView)
}

class RecordBoxView: UIView {

    @IBOutlet weak var rippleBut : RippleButton!
    @IBOutlet weak var waveform : WaveformView!
    @IBOutlet weak var bgImageView : UIImageView!
    @IBOutlet weak var timeView : UILabel!
    @IBOutlet weak var indicateView : UIView!
   
    @IBInspectable open var recordColor: UIColor = defaultRecordCollor
    @IBInspectable open var playColor: UIColor = defaultPlayCollor
    @IBInspectable open var pauseColor: UIColor = defaultPauseCollor
    @IBInspectable open var animationTime: Double = 0.9
    @IBInspectable open var backImage: UIImage? = nil
    
    weak var delegate: RecordBoxViewDelegate?
    var recordTime: TimeInterval = 0
    var playTime: TimeInterval = 0
    var state : Int!
    var recordingSession: AVAudioSession!
    var playingSession: AVAudioSession!
    var audioRecorder: AVAudioRecorder!
    fileprivate var audioFilename : URL!
    fileprivate var audioPlayer = AVAudioPlayer()
    fileprivate var timeTimer: Timer?
    
     // MARK: - Instance methods
    
    class func instanceFromNib() -> UIView {
        return UINib(nibName: "RecordBoxView", bundle: nil).instantiate(withOwner: nil, options: nil)[0] as! UIView
    }
  
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        NotificationCenter.default.addObserver(self, selector: #selector(RecordBoxView.trippleNotification), name: .trippleNotification, object: nil)
    }
    deinit {
        NotificationCenter.default.removeObserver(self, name: .trippleNotification, object: nil)
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        
        waveform.transform = CGAffineTransform(rotationAngle: (180.0 * CGFloat(Double.pi)) / 180.0)
        
        rippleBut.delegate = self
        audioFilename = getDocumentsDirectory().appendingPathComponent("recording.m4a")
        rippleBut.state = State.Record.rawValue
        updateRippleButton(recordColor, State.Record)
        rippleBut.animationTime = animationTime
        bgImageView.image = backImage
        rippleBut.isHidden = true
        recordingSession = AVAudioSession.sharedInstance()
        playingSession = AVAudioSession.sharedInstance()
        do {
            try recordingSession.setCategory(AVAudioSessionCategoryPlayAndRecord)
            try recordingSession.setActive(true)
            recordingSession.requestRecordPermission() { [unowned self] allowed in
                DispatchQueue.main.async {
                    if allowed {
                        self.rippleBut.isHidden = false
                    }
                }
            }
        } catch {
            // failed to record!
        }
        
        
        
    }

    // MARK: - Public methods
    
    func startRecording() {
     
        let settings = [
            AVFormatIDKey: Int(kAudioFormatMPEG4AAC),
            AVSampleRateKey: 12000,
            AVNumberOfChannelsKey: 1,
            AVEncoderAudioQualityKey: AVAudioQuality.high.rawValue
        ]
        
        do {
     
            clearTimer()
            timeTimer = Timer.scheduledTimer(timeInterval:1, target: self, selector: #selector(RecordBoxView.updateTime), userInfo: nil, repeats: true)
            
            audioRecorder = try AVAudioRecorder(url: audioFilename, settings: settings)
            audioRecorder.delegate = self as AVAudioRecorderDelegate
            audioRecorder.record()
        } catch {
            finishRecording()
        }
    }
    
    func finishRecording() {
        clearTimer()
        audioRecorder.stop()
    }
    func play() {
        timeTimer = Timer.scheduledTimer(timeInterval:1, target: self, selector: #selector(RecordBoxView.updateTime), userInfo: nil, repeats: true)
        audioPlayer.prepareToPlay()
        audioPlayer.play()
        waveform.startRunloop()
        waveform.isHidden = false
    }
    func pause() {
        audioPlayer.pause()
        waveform.stopRunloop()
        waveform.isHidden = true
    }
    
    
    
    //MARK: - Notification methods
    
    @objc func trippleNotification(_ obj : Notification) {
        let colorstate = Bool((indicateView.tag))
        indicateView.tag = Int(!colorstate)
        indicateView.backgroundColor = colorstate ? self.rippleBut.buttonColor : self.rippleBut.rippleColor
    }
    
    // MARK: - Private methods
    
    fileprivate func updateRippleButton(_ color : UIColor, _ param : State) {
        state = param.rawValue
        rippleBut.buttonColor = color
        rippleColor(color)
    }
    
    fileprivate func rippleColor(_ param : UIColor) {
        if let rgbColours = param.cgColor.components {
            rippleBut.rippleColor = UIColor.init(red: rgbColours[0], green: rgbColours[1], blue: rgbColours[2], alpha: 0.2)
        }
    }
    
    fileprivate func getDocumentsDirectory() -> URL {
        let paths = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
        let documentsDirectory = paths[0]
        return documentsDirectory
    }
    
    fileprivate func clearTimer() {
        timeTimer?.invalidate()
        timeTimer = nil
        timeView.text = "0:00"
    }
    
    @objc fileprivate func updateTime() {
        
        var duration : TimeInterval?

        if state == State.FinishRecord.rawValue {
            duration = audioRecorder.currentTime
            self.recordTime = duration!
        } else {
            duration = audioPlayer.currentTime
            self.playTime = duration!
        }
        let text = duration?.positionalTime
        timeView.text = text
        
        self.delegate?.callbackWithTime(text!, duration!)
        
    }
    
}
// MARK: - AVAudioPlayerDelegate methods

extension RecordBoxView : AVAudioPlayerDelegate {
    func audioPlayerDidFinishPlaying(_ player: AVAudioPlayer, successfully flag: Bool) {
        if flag {
            clearTimer()
            rippleBut.state = State.Pause.rawValue
            updateRippleButton(pauseColor, State.Play)
            rippleBut.animate(false)
            waveform.stopRunloop()
            waveform.isHidden = true
        }
    }
}
// MARK: - AVAudioRecorderDelegate methods

extension RecordBoxView : AVAudioRecorderDelegate {
    func audioRecorderDidFinishRecording(_ recorder: AVAudioRecorder, successfully flag: Bool) {
        if flag {
            do {
                try self.playingSession.setCategory(AVAudioSessionCategoryPlayback)
                try self.playingSession.setActive(true)
                self.audioPlayer = try AVAudioPlayer(contentsOf: self.audioFilename)
                self.audioPlayer.delegate = self
                waveform.start(&audioPlayer)
                waveform.stopRunloop()
                waveform.isHidden = true
                self.delegate?.callbackWithURL(self.audioFilename)
            } catch {
                // failed to play!
            }
        }
    }
}
// MARK: - RippleButtonDelegate methods

extension RecordBoxView : RippleButtonDelegate {
    
    func callbackClicked(_ obj : RippleButton){
        print(obj)
        
        if state == State.Record.rawValue {
            updateRippleButton(recordColor, State.FinishRecord)
            rippleBut.state = State.Record.rawValue
            rippleBut.animate(true)
            
            if delegate == nil {
                self.startRecording()
                return
            }
            self.delegate?.callbackBeginRecord(self)
         
        } else if state == State.FinishRecord.rawValue {
            rippleBut.state = State.Pause.rawValue
            updateRippleButton(playColor, State.Play)
            rippleBut.animate(false)
            if delegate == nil {
                self.finishRecording()
                return
            }
            self.delegate?.callbackEndRecord(self)
        } else if state == State.Play.rawValue {
            rippleBut.state = State.Play.rawValue
            updateRippleButton(playColor, State.Pause)
            rippleBut.animate(true)
            
            if delegate == nil {
                self.play()
                return
            }
            self.delegate?.callbackPlay(self)
        } else if state == State.Pause.rawValue {
            rippleBut.state = State.Pause.rawValue
            updateRippleButton(pauseColor, State.Play)
            rippleBut.animate(false)
            if delegate == nil {
                self.pause()
                return
            }
            self.delegate?.callbackPause(self)
        }
        
    }
}
