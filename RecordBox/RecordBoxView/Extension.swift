//
//  Helper.swift
//  DiffcoTest
//
//  Created by Sergiy Bekker on 06.11.17.
//  Copyright © 2017 SBApps. All rights reserved.
//

import UIKit
import SystemConfiguration

// MARK: - Int

extension Int {
    init(_ bool:Bool) {
        self = bool ? 1 : 0
    }
}
// MARK: - Bool

extension Bool {
    init(_ number: Int) {
        self.init(number as NSNumber)
    }
}


// MARK: - NSTimeInterval
extension TimeInterval {
    struct DateComponents {
        static let formatterPositional: DateComponentsFormatter = {
            let formatter = DateComponentsFormatter()
            formatter.allowedUnits = [.minute,.second]
            formatter.unitsStyle = .positional
            formatter.zeroFormattingBehavior = .pad
            return formatter
        }()
    }
    var positionalTime: String {
        return DateComponents.formatterPositional.string(from: self) ?? ""
    }
}

extension UIView {
    
    @IBInspectable var cornerRadius: CGFloat {
        
        get{
            return layer.cornerRadius
        }
        set {
            layer.cornerRadius = newValue
            layer.masksToBounds = newValue > 0
        }
    }
    
    @IBInspectable var borderWidth: CGFloat {
        get {
            return layer.borderWidth
        }
        set {
            layer.borderWidth = newValue
        }
    }
    
    @IBInspectable var borderColor: UIColor? {
        get {
            return UIColor(cgColor: layer.borderColor!)
        }
        set {
            layer.borderColor = borderColor?.cgColor
        }
    }
}

// MARK: Notification

extension Notification.Name {
    static let trippleNotification = Notification.Name("trippleNotification")
    
    
}
