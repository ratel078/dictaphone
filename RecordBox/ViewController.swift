//
//  ViewController.swift
//  RecordBox
//
//  Created by Sergiy Bekker on 17.11.17.
//  Copyright © 2017 SBApps. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    var recordBox : RecordBoxView!
        
    override func viewDidLoad() {
        super.viewDidLoad()
        recordBox =  RecordBoxView.instanceFromNib() as! RecordBoxView
        recordBox.frame = self.view.frame
        recordBox.delegate = self
        self.view.addSubview(recordBox)
    }
}

// MARK: - RecordBoxViewDelegate methods

extension ViewController : RecordBoxViewDelegate {

    func callbackWithTime(_ string : String, _ duration : TimeInterval) {
        //print(string)
        //print(duration)
    }
    
    func callbackWithURL(_ url : URL){
        print(url)
    }
    func callbackBeginRecord(_ obj: RecordBoxView) {
        recordBox.startRecording()
    }
    
    func callbackEndRecord(_ obj: RecordBoxView) {
        recordBox.finishRecording()
    }
    
    func callbackPlay(_ obj: RecordBoxView) {
        recordBox.play()
    }
    
    func callbackPause(_ obj: RecordBoxView) {
        recordBox.pause()
    }
    
    
}

